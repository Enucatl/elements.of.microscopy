Let's start from the \emph{differential wave equation}
	% -----------
	\begin{align}
		\frac{\partial^2 \psi}{\partial x^2} = \frac{1}{v^2} \frac{\partial^2 \psi}{\partial t^2},
		\label{eq:wave_equation}
	\end{align}
	% -----------
which is a partial differential equation describing a continuous system (see Exercise 1). This \eg can be the behaviour of a wave on a string (see \figp{2.2} from the lecture slides). In general the disturbance is moving and $\psi$ is a function of $x$ and $t$: $\psi = f (x,t)$. At $t=0$ we can write
	% -----------
	\begin{align*}
		\Bigl. \psi (x,t) \Bigr\vert_{t = 0} = f (x, 0) = f(x)
	\end{align*}
	% -----------
and $f$ is the \emph{profile} of the wave.

For a non-changing (\ie constant) profile over time and a coordinate system $S'$ that is moving at speed $v$, we can write:
	% -----------
	\begin{align*}
		\psi (x,t) = f (x - v t) = f(x') && \text{with} && x' = x - v t,
	\end{align*}
	% -----------
where $f$ can also be interpreted as the general form of a 1D wave equation.

We now have a closer look at \emph{harmonic waves} since any wave shape can be obtained with a superposition of harmonic waves (\see{cha:superposition_principle}). Assuming a static cosinusoidal profile first, we can write:
	% -----------
	\begin{align*}
		\Bigl. \psi (x,t) \Bigr\vert_{t = 0} = \psi (x) = A \cos (k x) = f(x),
	\end{align*}
	% -----------
where $A$ is the amplitude, $k$ the propagation number (since the cosine must be taken from a unit-less quantity). Thus, for a moving wave one obtains
	% -----------
	\begin{align}
		\psi (x,t) = A \cos \left[k (x - v t) \right],
		\label{eq:wave_func_sol}
	\end{align}
	% -----------
which represents the solution of \eq{eq:wave_equation} and is periodic in space and time. 

% ***************
	\begin{definition}
	The \emph{spatial period} is the wavelength, denoted by $\lambda$. This means that
		% -----------
		\begin{align}
			\psi (x,t) = \psi (x \pm \lambda,t).
		\end{align}
		% -----------
	For a harmonic wave this is equivalent to altering the argument of the sine function by $2 \pi$, which implies that $k = \frac{2 \pi}{\lambda}$.
	\end{definition}
% ***************

The latter implication becomes evident after following transformation:
	% -----------
	\begin{align*}
		\cos \bigl( k (x - v t) \bigr) = \cos \Bigl( k \bigl( (x \pm \lambda) - v t \bigr) \Bigr) = \cos \Bigl( k (x - v t) \pm 2\pi \Bigr),
	\end{align*}
	% -----------
meaning that $\vert k \lambda \vert = 2\pi$. We can now state following two definitions:

% ***************
	\begin{definition}
		The argument of the sine function in \eq{eq:wave_func_sol} is called \emph{phase}.
	\end{definition}
% ***************

% ***************
	\begin{definition}
		The amount of time it takes for one complete wave to pass a (stationary) observer is called the \emph{temporal period} $\tau$.
		\label{def:temp_period}
	\end{definition}
% ***************

\noindent
The above definition yields the following relation:
	% -----------
	\begin{align*}
		\psi (x, t) = \psi (x, t \pm \tau),
	\end{align*}
	% -----------
and in analogy to that:
	% -----------
	\begin{align*}
		\tau = \frac{\lambda}{v} && \text{with} && \vert k v \tau \vert = 2\pi && \text{and} && \nu = \frac{1}{\tau},
	\end{align*}
	% -----------
where $\tau$ represents the \emph{number of units of time per wave} and $\nu$ is the \emph{temporal frequency}. The latter one gives the \emph{number of waves per unit of time}.

With these relations we can define other important quantities such as the \emph{angular temporal frequency}:
	% -----------
	\begin{align}
		\boxed{\omega := \frac{2\pi}{\tau} = 2 \pi \nu}
	\end{align}
	% -----------
and the \emph{wave number} (or \emph{spatial frequency}), giving the number of waves per unit length:
	% -----------
	\begin{align}
		\boxed{\kappa := \frac{1}{\lambda}}
	\end{align}
	% -----------
