A plane wave is the simplest example of a three-dimensional wave. It exists at a given time, when all the surfaces on which a disturbance has a \emph{constant phase} form a set of planes, each perpendicular to the propagation direction $\vec{k}$ (see \fig{fig:plane_waves}). A mathematical expression for a plane that is perpendicular to a given vector $\vec{k}$ and that passes through some point $\vec{r}_0$ is given by:
	% -----------
	\begin{align}
		\boxed{(\vec{r} - \vec{r}_0) \cdot \vec{k} = 0}
		\label{eq:scalar_prod}
	\end{align}
	% -----------
where we force the vector $(\vec{r} - \vec{r}_0)$ to sweep out a plane perpendicular to $\vec{k}$.

%==================================================
\begin{figure}[ht]
	\centering
	\includegraphics[width=12cm]{graphics/plane_waves.jpg}
	\caption{(a) The Cartesian unit basis vectors. (b) A plane wave moving in $k$-direction.}
	\label{fig:plane_waves}
\end{figure}
%==================================================

We simplify above expression by first writing the vector $\vec{k}$ in Cartesian coordinates in terms of the unit basis vectors $(\vec{\hat{i}},\vec{\hat{j}},\vec{\hat{k}})$:
	% -----------
	\begin{align*}
		\vec{k} = k_x \vec{\hat{i}} + k_y \vec{\hat{j}} + k_z \vec{\hat{k}}.
	\end{align*}
	% -----------
Equation \eqref{eq:scalar_prod} can now be expressed in the form:
	% -----------
	\begin{align*}
		k_x (x - x_0) + k_y (y - y_0) + k_z (z - z_0) = 0,
	\end{align*}
	% -----------
or as:
	% -----------
	\begin{align*}
		k_x x + k_y y + k_z z = k_x x_0 + k_y y_0 + k_z z_0 = const.
	\end{align*}
	% -----------
The most concise form of the equation of a plane perpendicular to $\vec{k}$ is then just:
	% -----------
	\begin{align}
		\boxed{\vec{k} \cdot \vec{r} = const}
	\end{align}
	% -----------
This is a plane where position vectors each have the same projection onto the $\vec{k}$-direction.

We can then finally rewrite \eq{eq:complex_repres} to:
	% -----------
	\begin{align}
		\boxed{\psi (\vec{r}) = A e^{i \vec{k} \cdot \vec{r}}}
	\end{align}
	% -----------
which remains constant over every plane defined by $\vec{k} \cdot
\vec{r} = const$. Moreover, for plane waves,
	% -----------
	\begin{align*}
		\psi (\vec{r}) = \psi \left( \vec{r} + \lambda \frac{\vec{k}}{k} \right),
	\end{align*}
	% -----------
where $k$ is the magnitude of $\vec{k}$ and thus $\frac{\vec{k}}{k}$ its unit vector. By making use of the exponential notation introduced in \sect{cha:complex_representation} with
	% -----------
	\begin{align*}
		\left. A \cdot e^{i \vec{k} \cdot \vec{r}} \stackrel{!}{=} A
        \cdot e^{i \vec{k} \cdot \left( \vec{r} + \lambda \frac{\vec{k}}{k} \right)}
        = A \cdot e^{i \vec{k} \cdot \vec{r}} \cdot \underbrace{e^{i \lambda k}}_{\stackrel{!}{=} 1} \quad \right\rbrace \Longrightarrow \lambda k = 2\pi
	\end{align*}
	% -----------
we can derive the final form for the magnitude of the \emph{propagation vector}:
	% -----------
	\begin{align}
		\boxed{k = \frac{2\pi}{\lambda}}
	\end{align}
	% -----------
Finally \eq{eq:complex_repres} reads as
	% -----------
	\begin{align}
		\boxed{\psi (\vec{r}, t) = A e^{i \left( \vec{k} \cdot \vec{r} \mp \omega t\right)}}
	\end{align}
	% -----------
for A being the wave (disturbance) that travels along the $\vec{k}$-direction, with a given phase. 

At a given time the surface joining all points of equal phase are known as \emph{wavefronts}. A plane harmonic wave in 3D is given by:
	% -----------
	\begin{align}
		\psi (x,y,z,t) = A \cdot e^{i [k (\alpha x + \beta y + \gamma z) \mp \omega t]},
	\end{align}
	% -----------
where $\alpha, \beta, \gamma$ are the direction cosines of $\vec{k}$ with $\alpha^2 + \beta^2 + \gamma^2 = 1$.
