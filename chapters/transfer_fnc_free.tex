%==================================================
\begin{figure}[ht]
	\centering
	\includegraphics[width=10cm]{graphics/transferFunctionFreeSpace.jpg}
	\caption{Propagation of light between two planes is regarded as a linear system whose input and output are the complex amplitudes of the wave in the two planes}
	\label{fig:transferFunctionFreeSpace}
\end{figure}
%==================================================

The \underline{goal} is to determine the complex amplitude $g(x,y)$ at a given distance $d$, when the complex amplitude of the wave at the input plane $f(x,y) = U(x,y,0)$ is given.\\
It holds that:
\begin{itemize}
	\item $f(x,y)$ and $g(x,y)$ are input and output of linear systems.
	\item the system is linear since $U(x,y,z)$ satisfies the \emph{Helmholtz equation}, \see{cha:spherical_waves}.
	\item the system is shift-invariant because free space is invariant to displacement of the coordinate system:\\
	In general : $g(t) = \int_\infty^\infty h(t - \tau) f(\tau) \mathrm{d} \tau$
\end{itemize}
Thus, a linear shift-invariant system is characterized by its impulse function $h(x,y)$ or its \emph{transfer function} $H(\nu_x, \nu_y)$. The latter one is defined by the following: The function $H(\nu_x, \nu_y)$ is the factor by which an input spatial harmonic function of frequencies $\nu_x, \nu_y$ is multiplied to yield the output harmonic function.

In our case we have the \emph{input harmonic function}
	% -----------
	\begin{align*}
		f(x,y) = A \cdot e^{-i 2\pi (\nu_x x + \nu_y y)},
	\end{align*}
	% -----------
which corresponds to a plane wave
	% -----------
	\begin{align*}
		U(x,y,z) = A \cdot e^{-i (k_x x + k_y y + k_z z)}
	\end{align*}
	% -----------
evaluated at $z=0$. The output function reads as
	% -----------
	\begin{align*}
		U(x,y,d) = g(x,y) = A \cdot e^{-i (k_x x + k_y y + k_z d)}
	\end{align*}
	% -----------
and it follows that
	% -----------
	\begin{align}
		H(\nu_x, \nu_y) = \frac{g(x,y)}{f(x,y)} = e^{-i k_z d} \Longrightarrow \boxed{H(\nu_x, \nu_y) = \exp \left[-i 2\pi \sqrt{\frac{1}{\lambda^2} - \nu_x^2 - \nu_y^2} \cdot d\right]}
		\label{eq:transfer}
	\end{align}
	% -----------
The latter expression in \eq{eq:transfer} is called the \emph{transfer function of free space}. Relating to that we can now summarize the following:
\begin{itemize}
	\item A \emph{harmonic function} meeting the following conditions
	% -----------
	\begin{align*}
		\nu_x^2 + \nu_y^2 \leqslant \frac{1}{\lambda^2} \\
		\vert H(\nu_x, \nu_y) \vert = 1 \\
		\varphi = \arg\lbrace H(\nu_x, \nu_y) \rbrace
	\end{align*}
	% -----------
	undergoes a \emph{spatial phase shift} as it propagates, but its magnitude is not altered. This is the basis for \emph{free-space propagation-based} phase contrast!
	\item For
	% -----------
	\begin{align*}
		\nu_x^2 + \nu_y^2 > \frac{1}{\lambda^2}
	\end{align*}
	% -----------
	the exponent of $H(\nu_x, \nu_y)$ becomes real and the transfer function in \eq{eq:transfer} represents an attenuation! (evanescent wave)
\end{itemize}

\noindent
It is also worth-mentioning that the above considerations represent the foundation of \emph{Abbe's limit} in imaging, which states that the spatial bandwidth of light propagation in free-space is approximately $\frac{1}{\lambda}$. In other words: details that are of sizes finer than $\lambda$ cannot be transmitted by an optical wave of wavelength $\lambda$ over distances much greater than $\lambda$. We demonstrate this in the following

\textbf{Example:} Let's consider an arbitrary spatial frequency $\nu_\mathrm{p}$. If $\nu_\mathrm{p} = \sqrt{\nu_x^2 + \nu_y^2} \approx \frac{1}{\lambda}$ (so $\nu_\mathrm{p}$ exceeds $\frac{1}{\lambda}$ slightly), we can write
	% -----------
	\begin{align*}
		\exp \left[ - 2\pi \sqrt{\nu_\mathrm{p}^2 - \frac{1}{\lambda^2}} \cdot d \right] = \exp \left[ - 2\pi \sqrt{\nu_\mathrm{p} - \frac{1}{\lambda}} \cdot \sqrt{\nu_\mathrm{p} + \frac{1}{\lambda}} \cdot d \right] \approx \\
		\approx \underbrace{\exp \left[ - 2\pi \sqrt{\nu_\mathrm{p} - \frac{1}{\lambda}} \cdot \sqrt{\frac{2 d^2}{\lambda}}\right]}_{\text{with} \; \nu_\mathrm{p} - \frac{1}{\lambda} \approx \frac{\lambda}{2 d^2}} \approx e^{- 2\pi}
	\end{align*}
	% -----------
Following above relations, one can also show that
	% -----------
	\begin{align*}
		\frac{\nu_\mathrm{p} - \frac{1}{\lambda}}{\frac{1}{\lambda}} \approx \frac{1}{2} \left( \frac{\lambda}{d} \right)^2
	\end{align*}
	% -----------
and we can state that for $d \gg \lambda$, $\frac{1}{\lambda}$ can be considered as a ``cut-off frequency'' ($\nu_\mathrm{p} \approx \frac{1}{\nu}$) where the attenuation coefficient drops sharply. Therefore there is no transmission.
A detailed derivation of Abbe's limit is offered in Appendix \ref{cha:abbe.limit.calculation}.