Let us consider a complex function $f$, that, in the simplest case, depends only on one real variable $t$,
so that $f\:: \: \forall \: t \in \mathbb{R} \longrightarrow f(t) \in \mathbb{C}$. If the function is integrable
on the real line, meaning that
\begin{equation}
  \int_{-\infty}^{+\infty}dt\: |f(t)| < +\infty 
\end{equation}
then, the Fourier transform $\hat{f}(\omega)$ exists and is defined as
\begin{equation}
    \hat{f}(\omega) = \frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty} \de t \: f(t)
    e^{-i \omega t}
    \label{eq:fourier.definition}
\end{equation}
The factor of $1/\sqrt{2\pi}$ can be chosen differently in other sources.
We also indicate with $\Four$ the Fourier transform operator applied to the generic input $(\cdot)$, defined as:
\begin{equation}
    \Four(\cdot)(\omega) = \frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty} \de t \:
    e^{-i \omega t} (\cdot)
    \label{eq:fourier_operator.definition}
\end{equation}
Since, in our specific case, the function $f(t)$ corresponds to a physical signal, it holds that:
\begin{equation}
  \lim_{x\to\pm\infty} f(t) = 0
  \label{eq:physical_signal}
\end{equation}
This fact will turn useful, when demonstrating the first derivative property of the Fourier transform.
The extremes of integration are implied to be $-\infty$ and $+\infty$, when they are not specified.  


\subsection{Fundamental properties}
\begin{theorem}[Linearity]
    $\Four$ is a linear operator, meaning that, considered two generic function $f(t)$ and $g(t)$
    and two complex constants $\alpha$ and $\beta$, holds: $\Four(\alpha f(t) + \beta g(t))(\omega) = 
    \alpha\Four(f)(\omega) + \beta\Four(g)(\omega)$.
    \begin{proof}
        \begin{align*}
            \Four(\alpha f(t) + \beta g(t))(\omega) &= \frac{1}{\sqrt{2\pi}} \int \de t \:
                    e^{-i \omega t}(\alpha f(t) + \beta g(t)) \\
            &= \frac{1}{\sqrt{2\pi}} \int \de t \: e^{-i \omega t}(\alpha f(t)) +
               \frac{1}{\sqrt{2\pi}} \int \de t \: e^{-i \omega t}(\beta g(t)) \\
            &= \alpha \cdot \frac{1}{\sqrt{2\pi}} \int \de t \: e^{-i \omega t}f(t) +
               \beta \cdot \frac{1}{\sqrt{2\pi}} \int \de t \: e^{-i \omega t}g(t)\\
            &= \alpha\Four(f)(\omega) + \beta\Four(g)(\omega)
        \end{align*}
    \end{proof}
\end{theorem}
The terms $\alpha$ and $\beta$ can be taken out from the integrals, because they do not depend on the integration
variable $t$ (as they are constants). Linearity is a very powerful property that facilitates the usage of an operator.
For example, the operator $(\cdot)^{2}$ is not linear, since $(\alpha x +\beta y)^{2} \neq \alpha(x)^{2} + \beta(y)^{2}$.

\begin{theorem}[Complex conjugation]
    If $g(t) = \overline{f(t)}$ then $\hat{g}(\omega) = \overline{\hat{f}(-\omega)}$.
    \begin{proof}
        \begin{align*}
            \hat{g}(\omega) &= \frac{1}{\sqrt{2\pi}} \int \de t \: e^{-i \omega t} g(t) \\
            &= \frac{1}{\sqrt{2\pi}} \int \de t \: e^{-i \omega t} \: \overline{f(t)}\\
            &= \frac{1}{\sqrt{2\pi}} \int \de t \: \overline{e^{i \omega t}} \:\overline{f(t)}\\
            &= \frac{1}{\sqrt{2\pi}} \overline{\int \de t \: e^{-i (-\omega) t} f(t)} =
            \overline{\hat{f}(-\omega)}
        \end{align*}
    \end{proof}
\end{theorem}
    A particularly important case is for $f$ a real function, then of course
    $f = \bar{f}$ so that the Fourier transform with negative frequencies
    is completely determined by the Fourier transform with positive
    frequencies only, since $\hat{f}(-\omega) = \overline{\hat{f}(\omega)}$.
    This always happens \eg for electromagnetic fields or pixel values in an
    image.

    \begin{theorem}
        [Convolution]
        Let the convolution of two functions $f$ and $g$ be defined as the
        integral (the factor $\sqrt{2\pi}$ again depends on the convention):
        \begin{equation*}
            (f \star g)(t) := \frac{1}{\sqrt{2\pi}}\int f(t - s) g(s) \de s
        \end{equation*}
        Then the following holds
        \begin{equation*}
            \widehat{(f \star g)}(\omega) = \hat{f}(\omega)\hat{g}(\omega)
        \end{equation*}
        That is, the Fourier transform allows to calculate the convolution
        as a simple pointwise product.
        \begin{proof}
            \begin{align*}
                \widehat{(f \star g)}(\omega) &= \frac{1}{\sqrt{2\pi}} \int
                \de t e^{-i \omega t} \frac{1}{\sqrt{2\pi}} \int \de s f(t - s) g(s) \\
                &= \frac{1}{\sqrt{2\pi}} \int \de s \frac{1}{\sqrt{2\pi}} \int \de t e^{- i \omega (t - s)}
                e^{- i \omega s} f(t - s) g(s) \\
                &= \frac{1}{\sqrt{2\pi}} \int \de s e^{- i \omega s} g(s) \: \frac{1}{\sqrt{2\pi}} \int \de t e^{- i \omega (t - s)}
                f(t - s) \\
                &= \left(\frac{1}{\sqrt{2\pi}} \int \de s e^{- i \omega s} g(s)\right) \left(\: \frac{1}{\sqrt{2\pi}} \int \de t' e^{- i \omega t'}
                f(t')\right) \\
                &= \hat{g}(\omega)\hat{f}(\omega) = \hat{f}(\omega)\hat{g}(\omega)
            \end{align*}
            In the fourth passage, $t$ was substituted with $t'=t-s$ ($dt'=dt$), to eliminate the apparent dependence of the second
            integral on the variable $s$.
        \end{proof}
    \end{theorem}
    This important fact is a fundamental tool in image processing, where it
    is often needed to calculate convolutions, but Fourier transforms and
    products are more efficient. It is also used in the propagation of wave
    fronts, which can be expressed either as a convolution or as a product
    in Fourier space depending on what is more convenient for the
    calculation at hand.

    \begin{theorem}
        [Derivatives]
        There are two important relationships between the Fourier transform
        and the derivatives of a function.
        \begin{enumerate}
            \item $\widehat{\partial_t f}(\omega) = i \omega \hat{f}(\omega)$
            \item $\partial_\omega \hat{f}(\omega) = -i \widehat{t\!f}(\omega)$
        \end{enumerate}
        \begin{proof}
            \hspace{\fill}\\
            \begin{enumerate}
                \item 
                    \begin{align*}
                        \widehat{\partial_t f}(\omega) &= \frac{1}{\sqrt{2\pi}}
                        \int \de t e^{-i \omega t} \partial_t f(t) \\
                        &= \left[e^{-i \omega t}f(t)\right]_{-\infty}^{+\infty} - \frac{1}{\sqrt{2\pi}}
                        \int \de t \partial_t(e^{-i \omega t}) f(t)\\
                        &= 0 + i\omega \frac{1}{\sqrt{2\pi}}
                        \int \de t e^{-i \omega t} f(t) = i\omega
                        \hat{f}(\omega) \\
                    \end{align*}
                In the second passage, the integration by parts is adopted: $\int_{a}^{b} \de x u(x)\frac{dv(x)}{dx} = 
                [u(x)v(x)]_{a}^{b} - \int_{a}^{b}\de x \frac{du(x)}{dx}v(x)$; the first term, $\left[e^{-i \omega t}f(t)\right]_{-\infty}^{+\infty}$
                goes to zero, because the term $e^{-i \omega t}$ is oscillating between -1 and +1, whereas $f(t)$ satisfies (A.4), so 
                $e^{-i \omega t}f(t)$ goes to zero when $t \longrightarrow \pm \infty$. 
                \item
                    \begin{align*}
                    \partial_\omega \hat{f}(\omega) &= \partial_\omega \frac{1}{\sqrt{2\pi}}\int
                    \de t e^{-i \omega t} f(t) \\
                    &= \frac{1}{\sqrt{2\pi}} \int \de t \partial_\omega
                    e^{-i \omega t} f(t)\\
                    &= -i \frac{1}{\sqrt{2\pi}} \int \de t e^{-i \omega t}
                tf(t) = -i\widehat{t\!f}(\omega)
                    \end{align*}
            \end{enumerate}
        \end{proof}
    \end{theorem}
    These relationships allow to turn derivatives into simpler multiplicative
    factors in Fourier space, thus making the study of partial differential equations, such
    as the wave equation, generally easier.

    \subsection{The inverse Fourier transform}
    The Fourier transformation is also invertible with
    \begin{equation*}
        f(t) = \frac{1}{\sqrt{2\pi}} \int \de \omega e^{i \omega t}
        \hat{f}(\omega)
    \end{equation*}

    \subsection{Fourier transform in higher dimensions}
    The definition of the Fourier transform is easily extended to functions
    of more than one variable:
    If $\vec{x} = (x_1, x_2, \ldots, x_n) \in \mathbb{R}^{n}$, then the $n$-dimensional Fourier
    transform is
    \begin{equation*}
        \hat{f}(\vec{k}) = \frac{1}{(2\pi)^{n/2}} \int \de{^n x} e^{-i \vec{k}
        \cdot \vec{x}}f(\vec{x})
    \end{equation*}
    The vector $\vec{k} = (k_1, k_2, \ldots, k_n) \in \mathbb{R}^{n}$, $\int\de{^nx} = \int\de x_{1}\cdots\int\de x_{n}$
    ($\int\de{^nx}$ is just a compact notation to indicate $n$ 1D integrations) and $\vec{k}\cdot\vec{x}$ is a scalar product, 
    that is computed on two vectors and gives as outcome a scalar:
    $\vec{k}\cdot\vec{x} = k_{1}x_{1} + \cdots + k_{n}x_{n} \in \mathbb{C}$.
