A characteristic intrinsic property of waves is that they ``interfere''. When two (or more) optical waves are present simultaneously in the same region of space, the overall wave function is the sum of the individual wave functions.

Let's consider the sum of two waves with complex amplitudes given by:
	% -----------
	\begin{align*}
		U(\vec{r}) = U_1 (\vec{r}) + U_2 (\vec{r}).
	\end{align*}
	% -----------
If we calculate the optical intensity of such a wave we get:
	% -----------
	\begin{align*}
		I = \vert U \vert^2 = \vert U_1 + U_2 \vert^2 = \vert U_1 \vert^2 + \vert U_2 \vert^2 + U_1^{*} U_2 + U_1 U_2^{*}.
	\end{align*}
	% -----------
By omitting the dependency on $\vec{r}$ we can write:
	% -----------
	\begin{align*}
		U_1 = \sqrt{I_1} e^{i \varphi_1} && U_2 = \sqrt{I_2} e^{i \varphi_2},
	\end{align*}
	% -----------
where $\varphi_1$ and $\varphi_2$ are the phases of the respective waves. It follows then (using complex properties):
	% -----------
	\begin{align}
		\boxed{I = I_1 + I_2 + 2 \sqrt{I_1 I_2} \cos (\varphi_2 - \varphi_1)}
		\label{eq:interference_eq}
	\end{align}
	% -----------
Thereby \eq{eq:interference_eq} is called \emph{interference equation} (see \fig{fig:interference_equation}). Looking at the phasor diagram one can see that the magnitude of the phasor $U$ is sensitive to the phase difference $\Delta \varphi = \varphi_2 - \varphi_1$ and not only to the magnitudes of the constituent phasors!

%==================================================
\begin{figure}[ht]
	\centering
	\includegraphics[width=12cm]{graphics/interference_equation.jpg}
	\caption{(a) Phasor diagram for the superposition of two waves of intensities $I_1$ and $I_2$ and phase difference $\Delta \varphi = \varphi_2 - \varphi_1$. (b) Dependence of the total intensity $I$ on the phase difference $\Delta \varphi$}
	\label{fig:interference_equation}
\end{figure}
%==================================================

Let's have a closer look at the interference equation:
\begin{enumerate}
	\item The intensity of the sum of the two waves is \underline{not} the sum of their intensities; there is an additional term attributed to \underline{interference}.
	\item The term might be positive (\emph{constructive}) or negative (\emph{destructive}) interfering.
\end{enumerate}

For a better illustration we look at three different values of $\Delta
\varphi = (\varphi_2 - \varphi_1)$, for $I_1 = I_2 := I_0$:
	% -----------
	\begin{align*}
	\begin{array}{ll}
		\Delta \varphi = 0 & \Longrightarrow\qquad I = 2 I_0 (1 + \cos (\Delta\varphi)) = 4 I_0 \\
		\Delta \varphi = \pi & \Longrightarrow\qquad I = 0 \\
		\Delta \varphi = \frac{\pi}{2} & \Longrightarrow\qquad I = 2 I_0
	\end{array}
	\end{align*}
	% -----------
We can thus state: The strong dependency of the intensity I on the phase difference $\varphi$ permits us to measure \underline{phase differences} by \underline{light intensity}.

Interference is not observed under ordinary lighting conditions since the random fluctuations of phases $\varphi_1$ and $\varphi_2$ (uniform distribution between 0 and $2\pi$) cause the cosine term to vanish! Light with such randomness is said to be ``partially coherent''.

Generally, interference is accomplished by a spatial redistribution of the
optical intensity \underline{without} violation of energy conservation.

\vspace{.5\baselineskip}
\noindent
\textbf{Example: } two waves might have uniform intensities $I_1$ and $I_2$
in some planes, but as a result of a position-dependent phase difference
$\Delta \varphi$ the total intensity might be smaller than $I_1 + I_2$ at
some positions and greater than $I_1 + I_2$ at others, with the total energy (integral of the intensity) being conserved.

%==================================================
\begin{figure}[ht]
	\centering
	\includegraphics[width=8cm]{graphics/waveBeforeMach.jpg}
	\caption{Dependence of the intensity $I$ of the superposition of two waves, each of intensity $I_0$, on the delay distance $d$. When the delay distance is a multiple of $\lambda$, the interference is \emph{constructive}; when it is an odd multiple of $\lambda / 2$ the interference is destructive}
	\label{fig:waveBeforeMach}
\end{figure}
%==================================================

	% -----------
	\begin{align*}
		U_1 &= \sqrt{I_0} e^{i k z} \\
		U_2 &= \sqrt{I_0} e^{i k z - k d} \\
		\Longrightarrow I &= 2 I_0 \bigl( 1 + \cos \underbrace{\left( \Delta \varphi \right)}_{\frac{2 \pi d}{\lambda}} \bigr)
	\end{align*}
	% -----------
So if the delay $d$ is an integer multiple of $\lambda$ we get \emph{constructive interference}. If $d$ is an odd integer multiple of $\frac{\lambda}{2}$, one obtains \emph{destructive interference}. In the following we show the principle of an \emph{interferometer}:

%==================================================
\begin{figure}[ht]
	\centering
	\includegraphics[width=8cm]{graphics/machZehnderInterferometer1}
	\caption{Sketch of a Mach-Zehnder-Interferometer. Source: Wikipedia}
	\label{fig:machZehnderInterferometer1}
\end{figure}
%==================================================

\noindent
For the interferometer the intensity $I$ is sensitive to
	% -----------
	\begin{align*}
		\Delta \varphi = \frac{2\pi d}{\lambda} = \frac{2\pi n d}{\lambda_0}
        \qquad \text{with } n := \frac{\lambda_0}{\lambda}
	\end{align*}
	% -----------
We can exemplify that with some real values, as for instance:
	% -----------
	\begin{align*}
		\frac{d}{\lambda_0} = 10^4 \quad \text{and} \quad \Delta n = 10^{-4} && \Longrightarrow && \Delta \varphi = 2\pi.
	\end{align*}
	% -----------
We now work out one more example.
\vspace{.5\baselineskip}

Consider the interference of two plane waves of equal intensity. One propagates in the $z$-direction and is given by $U_1 = \sqrt{I_0} e^{-i k z}$. The other one propagates at an angle $\theta$ with the $z$-axis, in the $x$-$y$-plane and is given by: $U_2 = \sqrt{I_0} \exp [- i(k \cos \theta z + k \sin \theta x)]$. At $z = 0$ the two waves interfere. The intensity pattern is described in the following:



%==================================================
\begin{figure}[ht]
	\centering
	\includegraphics[width=14cm]{graphics/machZehnderInterferometer2.jpg}
	\caption{The interference of two plane waves at an angle $\theta$ results in a sinusoidal intensity pattern of period $\lambda / \sin \theta$}
	\label{fig:machZehnderInterferometer2}
\end{figure}
%==================================================

	% -----------
	\begin{align*}
		\bigl. U \bigr\vert_{z=0} = U_1 + U_2 = \sqrt{I_0} e^{i k 0} + \sqrt{I_0} e^{i k sin \theta x} = \left[ 1 + e^{i k \sin \theta x} \right] \sqrt{I_0}.
	\end{align*}
	% -----------
The \emph{intensity} is then given by
	% -----------
	\begin{align*}
		I = \vert U \vert^2 = I_0 \left[ 1 + e^{i k \sin \theta x} \right]^2 = 2 I_0 [1 + \cos(k \sin \theta x)],
	\end{align*}
	% -----------
which is a periodic pattern with period $\frac{2\pi}{k \sin \theta} = \frac{\lambda}{\sin \theta}$.

\underline{Example 1:} Method for printing a sinusoidal pattern of high resolution for use as a a diffraction pattern.

\underline{Example 2:} Method to monitor the angle of arrival $\theta$ of a wave by mixing it with a reference wave and record the intensity distribution $\longrightarrow$ Holography!

