The idea behind Fresnel diffraction is to multiply the incident wave by the aperture function, \underline{but} then propagate the resulting beam according to \emph{Fresnel approximation}. Before we continue, we have to remind that
	% -----------
	\begin{align}
		h(x,y) \approx h_0 \cdot e^{- i k \frac{x^2 + y^2}{2 d}}
	\end{align}
	% -----------
is the \emph{impulse-response function} of free space propagation in the Fresnel approximation. If we consider then free-space propagation we can generally write
	% -----------
	\begin{align}
		g(x,y) = \int\limits_{-\infty}^{+\infty} \int\limits_{-\infty}^{+\infty} f(x',y') h(x-x', y-y') \mathrm{d} x' \mathrm{d} y'
	\end{align}
	% -----------
and by incorporating the \emph{Fresnel approximation} we get
	% -----------
	\begin{align*}
		g(x,y) = h_0 \cdot \int\limits_{-\infty}^{+\infty} \int\limits_{-\infty}^{+\infty} f(x',y') \exp \left[ - i \pi \frac{(x-x')^2 + (y-y')^2}{\lambda d} \right] \mathrm{d} x' \mathrm{d} y'.
	\end{align*}
	% -----------
In Fresnel approximation the diffraction pattern after the aperture (for incident intensity $I_\mathrm{i}$) at a distance $d$ is given by
	% -----------
	\begin{align}
		\boxed{I(x,y) = \frac{I_\mathrm{i}}{(\lambda d)^2} \left\vert \int\limits_{-\infty}^{+\infty} \int\limits_{-\infty}^{+\infty} p(x',y') \exp \left[ - i \pi \frac{(x-x')^2 + (y-y')^2}{\lambda d} \right] \mathrm{d} x' \mathrm{d} y'\right\vert^2}
		\label{eq:fresnel_approx}
	\end{align}
	% -----------
where $x'$ and $y'$ are the coordinates in the aperture plane.

Before finally summarizing the results we first introduce some simplifications. We normalize the distances using $\sqrt{\lambda d}$ as a unit of distance. We then get
	% -----------
	\begin{align*}
		X := \frac{x}{\sqrt{\lambda d}} && \text{and} && Y := \frac{y}{\sqrt{\lambda d}}
	\end{align*}
	% -----------
as well as analog values for $X'$ and $Y'$. From \eq{eq:fresnel_approx} it follows then
	% -----------
	\begin{align}
		\boxed{I(x,y) = I_\mathrm{i} \left\vert \int\limits_{-\infty}^{+\infty} \int\limits_{-\infty}^{+\infty} p(X',Y') e^{- i \pi \left[ (X-X')^2 + (Y-Y')^2 \right]} \mathrm{d} X' \mathrm{d} Y'\right\vert^2}
	\end{align}
	% -----------
where the exponential function represents an oscillating function. For its components ($\cos \pi x^2$ and $\sin \pi x^2$) we can state the following:
\begin{itemize}
	\item they are oscillating at an increasing frequency.
	\item their first lobes lie in the intervals: $\vert x\vert < \frac{1}{\sqrt{2}}$ (for the cosine) and $\vert x\vert < 1$ (for sine function).
	\item the contribution comes only from the first few lobes, subsequent lobes cancel each other out.
	\item the result of the convolution is covered by the Fresnel number $n_\mathrm{F} = \frac{a^2}{\lambda d}$.
\end{itemize}

\noindent
Finally we can state the following to cases:
\begin{itemize}
	\item For $d \rightarrow 0$ and $N_\mathrm{F} \rightarrow \infty$ (which is equivalent to $\lambda \rightarrow 0$, \ie ``ray optics''), the output is just the geometric shadow of the aperture.
	\item As $N_\mathrm{F}$ becomes smaller, the higher spatial frequency components are lost and one ends up with $N_\mathrm{F} \ll 1$, eventually Fraunhofer pattern.
\end{itemize}

As an example we look at an diffraction example after rectangular slits, as shown in \fig{fig:slide19}.

%==================================================
\begin{figure}[ht]
	\centering
	\includegraphics[width=\linewidth]{graphics/slide19.pdf}
	\caption{Fresnel diffraction from a slit}
	\label{fig:slide19}
\end{figure}
%==================================================

\clearpage