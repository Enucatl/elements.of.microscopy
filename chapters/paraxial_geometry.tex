In the following we take the generic scalar wave function as the starting point, \eg describing the electrical field $E$:
	% -----------
	\begin{align*}
		E (\vec{r},t) = E (\vec{r}) e^{i \omega t},
	\end{align*}
	% -----------
that satisfies the Helmholtz equation
	% -----------
	\begin{align*}
		\left( \nabla^2 + k^2 \right) E (\vec{r}) = 0
	\end{align*}
	% -----------
with:
	% -----------
	\begin{align*}
		\left\vert E (\vec{r}) \right\vert &\Longrightarrow \text{``amplitude''}\\
		\arg \left( E (\vec{r}) \right) &\Longrightarrow \text{``phase''}\\
	\end{align*}
	% -----------
We are now able to introduce the following
% ***************
	\begin{definition}[paraxial wave]
	A wave is called \emph{paraxial} if the wavefront's normals are paraxial rays, \ie such rays make a small angle to the optical axis of the system. For the case (illustrative) of refraction at a spherical interface, this means that the rays arrive at shallow angles.
	\end{definition}
% ***************
A paraxial wave is constructed in two steps:
\begin{itemize}
	\item start with a plane wave $A e^{-i k z}$ as a carrier.
	\item modulate its complex envelope $A$ slowly so that
		% -----------
		\begin{align*}
			E (\vec{r}) = A &(\vec{r}) e^{-i k z}\\
			&\text{\drsh variations of $A (\vec{r})$ are ``slow'' within $\lambda = \frac{2\pi}{k}$} 
		\end{align*}
		% -----------
\end{itemize}
\textbf{Explanation:} Mathematically, ``slow variation'' of $A (\vec{r})$ over a distance $\lambda$ can be expressed by:
	% -----------
	\begin{align*}
		\frac{\partial A}{\partial z} \ll \frac{A}{\lambda} \quad\longrightarrow \quad \frac{\partial A}{\partial z} \ll k A 
	\end{align*}
	% -----------
In addition to that, the derivative $\frac{\partial A}{\partial z}$ also varies ``slowly'' so that we can write:
	% -----------
	\begin{align*}
		\frac{\partial^2 A}{\partial z^2} \ll k \frac{\partial A}{\partial z} \quad\longrightarrow \quad \frac{\partial^2 A}{\partial z^2} \ll k^2 A 
	\end{align*}
	% -----------
As a consequence the \emph{paraxial Helmholtz equation} simplifies to:
	% -----------
	\begin{align}
		\boxed{\nabla_\mathrm{T}^2 A - 2 i k \frac{\partial A}{\partial z} = 0} && \text{with} && \nabla_\mathrm{T}^2 = \frac{\partial^2}{\partial x^2} + \frac{\partial^2}{\partial y^2}.
	\end{align}
	% -----------
A very interesting solution to this equation is the ``Gaussian beam'' where
	% -----------
	\begin{align*}
		A(\vec{r}) = \frac{A_1}{q(z)} \exp \left[ - i k \frac{\rho^2}{2 q(z)} \right] && \text{with} && \parbox[c][][c]{4cm}{$q(z) = z + i z_0$ \\ $\rho^2 = x^2 + y^2$}
	\end{align*}
	% -----------
Finally one obtains the following expression for the final wave:
	% -----------
	\begin{align}
		\boxed{E (\vec{r}) = A_0 \frac{w_0}{w (z)} \exp \left( - \frac{\rho^2}{w^2 (z)} \right) \exp \left[ -i \left( k z - \frac{k \rho^2}{2 R(z)} + \zeta (z)\right) \right]}
	\end{align}
	% -----------
Thereby the respective values are given by following equations
	% -----------
	\begin{align*}
		w (z) &= w_0 \left[ 1 + \left( \frac{z}{z_0} \right)^2 \right]^{\frac{1}{2}} \Longrightarrow \text{beam waist/radius with } w_0 = \sqrt{\frac{\lambda z_0}{\pi}}\\
		R (z) &= z \left[ 1 + \left( \frac{z_0}{z} \right)^2 \right] \Longrightarrow \text{curvature radius of the wavefront}\\
		\zeta (z) &= \tan^{-1} \left( \frac{z}{z_0} \right) \Longrightarrow \text{Gouy phase}\footnotemark \\
		\theta_0 &= \frac{\lambda}{\pi w_0} \Longrightarrow \text{beam divergence (\see{cha:beam_parameters})}
	\end{align*}
	% -----------
	\footnotetext{This phase shifts by $\pi$ as the beam passes through the focus in addition to the normal change in phase as the beam propagates.}

	