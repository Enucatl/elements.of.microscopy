A complex number $z$ is defined by
	% -----------
	\begin{align}
		z = x + i y,
		\label{eq:complex_number}
	\end{align}
	% -----------
where $x$ is the \emph{real}, $y$ the \emph{complex} part and $i := \sqrt{-1}$.

According to the \emph{Argand plane} in Polar coordinates (see \fig{fig:argand_diagram}) we can rewrite the above equation by:
	% -----------
	\begin{equation}
		\left.\begin{aligned}
		x &= r \cos \theta \\
		y &= r \sin \theta
		\end{aligned}
		\right\}
		\Longrightarrow z = x + i y = r (\cos \theta + i \sin \theta).
	\end{equation}
	% -----------

%==================================================
\begin{figure}[ht]
	\centering
	\includegraphics[width=10cm]{graphics/argand_plane.jpg}
	\caption{An \emph{Argand diagram} is a representation of a complex number in terms of its real and imaginary components. This can be done using either (a) $x$ and $y$ or (b) $r$ and $\theta$. Moreover, when $\theta$ is a constantly changing function of time (d), the arrow rotates at a rate $\omega$}
	\label{fig:argand_diagram}
\end{figure}
%==================================================
	
Making use of the \emph{Euler formula} $e^{i \theta} = (\cos \theta + i \sin \theta)$ \eq{eq:complex_number} finally reads as
	% -----------
	\begin{align}
		z = r e^{i \theta},
		\label{eq:complex_number2}
	\end{align}
	% -----------
where $r = \sqrt{x^2 + y^2}$ and $\theta = \arctan \left( \frac{y}{x} \right)$. The sine and cosine functions can then be expressed by:
	% -----------
	\begin{align*}
		\cos \theta = \frac{e^{i \theta} + e^{-i \theta}}{2} && \text{and} && \sin
        \theta = \frac{e^{i \theta} - e^{-i \theta}}{2 i}.
	\end{align*}
	% -----------
According to these relations and recalling the parity of the cosine function, we can rewrite \eq{eq:wave_func_sol} to:
	% -----------
	\begin{align*}
		\psi (x, t) = A \cos (kx - \omega t) = A \cos (\omega t - k x) = \text{Re} \left[ A e^{i (\omega t - k x)} \right].
	\end{align*}
	% -----------

\noindent
\underline{Note:} The complex notation is used only in computations! Once arrived at a final result, and only if we want to represent the actual wave, we must take the real part (\ie it is the physically relevant one). It is therefore quite common to use the \emph{complex representation of waves} by writing:
	% -----------
	\begin{align}
		\boxed{\psi (x, t) = A e^{i (\omega t - k x)} = A e^{i \varphi}}
		\label{eq:complex_repres}
	\end{align}
	% -----------
