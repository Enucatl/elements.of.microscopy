%==================================================
\begin{figure}[ht]
	\centering
	\includegraphics[width=12cm]{graphics/opticalFourierTransform.jpg}
	\caption{When the distance $d$ is sufficiently long, the complex amplitude at point $(x,y$ in the $z = d$ plane is proportional to the complex amplitude of the plane-wave component with angles $\theta_x \approx x / d \approx \lambda \nu_x$ and $\theta_y \approx y / d \approx \lambda \nu_y$, \ie, to the Fourier transform $F (\nu_x, \nu_y)$ of $f(x,y)$, with $\nu_x = x / \lambda d$ and $\nu_y = y / \lambda d$}
	\label{fig:opticalFourierTransform}
\end{figure}
%==================================================

We saw earlier that a plane wave that is incident on an element with a transmittance $f(x,y)$ gets deflected at angles $\theta_x$, $\theta_y$ with a weighting factor $F(\nu_x,\nu_y)$, \ie the Fourier transform of $f(x,y)$. It follows that one just has to produce a transparency that represents $f$ and illuminate it with a plane wave along $z$ in order to find the Fourier transform of a function $f(x,y)$. The question is then how to separate the various components from each other. It turns out that one just has to put a screen at very large distance $d$.

Namely, far enough, we can write:
	% -----------
	\begin{align}
		\boxed{g(x,y) \approx h_0 F \biggl( \frac{x}{\lambda d}, \frac{y}{\lambda d} \biggr)} && \text{with} && h_0 = \frac{i}{\lambda d} e^{- i k d}
	\end{align}
	% -----------
being the \emph{Fraunhofer Approximation for Free Space Propagation} (for the proof, see Saleh/Teich p.123). Finally we can summarize:

If the propagation is long enough, the only plane wave that contributes to the complex amplitude at a point $(x,y)$ in the output plane is the wave with directions making angles $\theta_x \approx \frac{x}{d}$ and $\theta_y \approx \frac{y}{d}$ with the optical axis. Contributions of all other waves cancel out as a result of destructive interference.

In Fraunhofer approximation the complex amplitude $g(x,y)$ of a wave of wavelength $\lambda$ in the $z = d$ plane is proportional to the Fourier transform, $F(\nu_x, \nu_y)$ of the complex amplitude $f(x,y)$ in the $z=0$ plane, evaluated at the spatial frequencies $\nu_x = \frac{x}{\lambda d}$ and $\nu_y = \frac{y}{\lambda d}$.

If $f(x, y)$ is confined to a small area of radius $b$ and if the distance
$d$ is sufficiently large, then the Fresnel number with respect to the
input plane $N_\mathrm{F}' = b^2/ \lambda d$
is small, \ie:

\begin{equation*}
    \boxed{N_\mathrm{F}' \ll 1} \qquad \text{condition of validity of Fraunhofer
    approximation.}
\end{equation*}

