In the following, the idea is to multiply the incident wave by the aperture function and then use \emph{Fraunhofer approximation} to determine the free space propagation.

We begin by writing the incident wave
	% -----------
	\begin{align*}
		U(x,y) = \sqrt{I_\mathrm{i}},
	\end{align*}
	% -----------
which is then multiplied with the aperture function $\rho (x,y)$
	% -----------
	\begin{align*}
		f(x,y) = \sqrt{I_\mathrm{i}} \rho (x,y) && \text{with} && \rho (x,y) = \begin{cases} 1 \quad \text{inside aperture} \\0 \quad \text{outside aperture}
		\end{cases}
	\end{align*}
	% -----------
The diffraction pattern then reads as
	% -----------
	\begin{align*}
		I (x,y) = \frac{I_\mathrm{i}}{(\lambda d)^2} \Biggl\vert P \biggl( \frac{x}{\lambda d}, \frac{y}{\lambda d} \biggr) \Biggr\vert && \text{with} && P = \mathfrak{F}\lbrace \rho (x,y) \rbrace \quad \text{(Fourier transform)}.
	\end{align*}
	% -----------
In words: The Fraunhofer diffraction pattern of the point $(x,y)$ is proportional to the squared magnitude of the Fourier transform of the aperture function $\rho (x,y)$ at the frequencies $\nu_x = \frac{x}{\lambda d}$, $\nu_y = \frac{y}{\lambda d}$.

In the following we report on two examples, without proof.

\subsubsection*{Example 1: Fraunhofer diffraction from a rectangular aperture}

%==================================================
\begin{figure}[ht]
	\centering
	\includegraphics[width=14cm]{graphics/fraunhoferDiffraction.jpg}
	\caption{Fraunhofer diffraction from a rectangular aperture. The central lobe of the pattern has half-angular widths $\theta_x = \lambda / D_x$ and $\theta_y = \lambda / D_y$}
	\label{fig:fraunhoferDiffraction}
\end{figure}
%==================================================

The intensity is then given by:
	% -----------
	\begin{align}
		I (x,y) = I_0 \cdot \text{sinc}^2 \left( \frac{D_x x}{\lambda d} \right) \cdot \text{sinc}^2 \left( \frac{D_y y}{\lambda d} \right)
	\end{align}
	% -----------
and the first zero is at
	% -----------
	\begin{align*}
		x = \pm \frac{\lambda d}{D_x}, && y = \pm \frac{\lambda d}{D_y} && \text{with} && I_0 = \left( \frac{D_x D_y}{\lambda d} \right)^2 \cdot I_\mathrm{i}.
	\end{align*}
	% -----------
The angular divergence of a diffracted light is
	% -----------
	\begin{align*}
		\theta_x = \frac{\lambda}{D_x} && \text{and} && \theta_y = \frac{\lambda}{D_y}.
	\end{align*}
	% -----------
	
	
	
\subsubsection*{Example 2: Fraunhofer diffraction from a circular aperture}
The intensity is then given by:
	% -----------
	\begin{align}
		I (x,y) = I_0 \cdot \left[ \frac{2 J_1 \left( \frac{\pi D \rho}{\lambda d} \right)}{\left( \frac{\pi D \rho}{\lambda d} \right)} \right]^2 && \text{with} && \rho = \sqrt{x^2 + y^2},
	\end{align}
	% -----------
where $J_1$ is the \emph{Bessel function of 1st order} and $I_0$ is given by
	% -----------
	\begin{align*}
		I_0 = \left( \frac{\pi D^2}{4 \lambda d} \right)^2 \cdot I_\mathrm{i}.
	\end{align*}
	% -----------
The resulting pattern is the so-called \emph{Airy pattern}. The radius of the \emph{Airy disc} is $\rho_\mathrm{S} = \frac{1.22 \lambda d}{D}$ or
	% -----------
	\begin{align}
		\boxed{\theta = 1.22 \frac{\lambda}{D}} \qquad \text{(half-angle subtended by the Airy disc).}
	\end{align}
	% -----------
Finally we note the following: Focusing light through a circular lens of diameter $D$ can be considered as having an aperture in front of an extended lens. Thus the resulting focus spot is an Airy pattern!

%==================================================
\begin{figure}[ht]
	\centering
	\includegraphics[width=14cm]{graphics/airyPattern.jpg}
	\caption{The Fraunhofer diffraction pattern from a circular aperture produces the Airy pattern with the radius of the central disk subtending an angle $\theta = 1.22 \lambda / D$}
	\label{fig:airyPattern}
\end{figure}
%==================================================
