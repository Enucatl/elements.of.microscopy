The plane-wave components that constitute a wave may also be separated by
use of a lens. A thin spherical lens transforms a plane wave into a
paraboloidal wave focused to a point in the lens focal plane. If the plane
wave arrives at small angles $\theta_x$ and $\theta_y$, the paraboloidal
wave is centered about the point $(\theta_x f,\theta_y f)$, where $f$ is
the focal length. The lens therefore maps each direction
$(\theta_x,\theta_y)$ into a single point $(\theta_x f,\theta_y f)$ in the
focal plane and thus separates the contributions of the different plane
waves.

Let $f(x, y)$ be the complex amplitude of the optical wave in the $z=0$
plane. Light is decomposed into plane waves, with the wave traveling at
small angles $\theta_x=\lambda \nu_x$ and $\theta_y=\lambda \nu_y$ having a
complex amplitude proportional to the Fourier transform $F(\nu_x,\nu_y)$.
This wave is focused by the lens into a point $(x,y)$ in the focal plane
where $x=\theta_x f=\lambda f \nu_x$, $y=\theta_y f=\lambda f \nu_y$. The
complex amplitude at point $(x, y)$ in the output plane is therefore
proportional to the Fourier transform of $f(x,y)$ evaluated at
$\nu_x=x/\lambda f$ and  $\nu_y=y/\lambda f$ so that (see exercise):
	% -----------
	\begin{align}
		\boxed{g(x,y) = h_\mathrm{L} F \biggl( \frac{x}{\lambda d},
        \frac{y}{\lambda d} \biggr)} \qquad \text{with} \quad h_\mathrm{L} =
        \frac{i}{\lambda f} e^{- i 2 k f} \text{ and } f \text{ the
        focal length.}
	\end{align}


%==================================================
\begin{figure}[ht]
	\centering
	\includegraphics[width=12cm]{graphics/lensTransferFunction1.jpg}
	\caption{Focusing of the plane waves associated with the harmonic Fourier components of the input function $f(x,y)$ into points in the focal plane. The amplitude of the plane wave with direction $(\theta_x, \theta_y) = (\lambda \nu_x, \lambda \nu_y)$ is proportional to the Fourier transform $F(\nu_x, \nu_y)$ and is focused at the point $(x,y) = (\theta_x f, \theta_y f) = (\lambda f \nu_x, \lambda f \nu_y)$}
	\label{fig:lensTransferFunction1}
\end{figure}
%==================================================

%==================================================
\begin{figure}[ht]
	\centering
	\includegraphics[width=10cm]{graphics/lensTransferFunction2.jpg}
	\caption{Fourier transform system. The Fourier component of $f(x,y)$ with spatial frequencies $\nu_x$ and $\nu_y$ generates a plane wave at angles $\theta_x = \lambda \nu_x$ and $\theta_y = \lambda \nu_y$ and is focused by the lens to the point $(x,y) = (f \theta_x, f \theta_y)$ so that $g(x,y)$ is proportional to the Fourier transform $F(x / \lambda f, y / \lambda f)$}
	\label{fig:lensTransferFunction2}
\end{figure}
%==================================================

\underline{In words}: The complex amplitude of light at a point $(x,y)$ in the back focal plane of a lens of focal length $f$ is proportional to the Fourier transform of the complex amplitude in front focal plane evaluated at the frequencies $\nu_x = \frac{x}{\lambda d}$ and $\nu_y = \frac{y}{\lambda d}$. This holds for Fresnel approximation.

Remember: each ray (with direction $\theta_x$, $\theta_y$) is mapped onto a point $(x,y) = (\theta_x f, \theta_y f)$.

\clearpage
