For simplicity reasons we now move to spherical coordinates (see \fig{fig:spherical_coord}). \emph{Spherical waves} are by definition spherically symmetrical, \ie not dependent on the angles $\theta$ and $\phi$:
	% -----------
	\begin{align*}
		\psi (\vec{r}) = \psi (r, \theta, \phi) = \psi (r)
	\end{align*}
	% -----------
	
%==================================================
\begin{figure}[ht]
	\centering
	\includegraphics[width=8cm]{graphics/spherical_coord.jpg}
	\caption{The geometry of spherical coordinates.}
	\label{fig:spherical_coord}
\end{figure}
%==================================================	
	
For this particular case, the Laplacian of $\psi(r)$ becomes
	% -----------
	\begin{align}
		\nabla^2 \psi (\vec{r}) = \frac{1}{r^2} \frac{\partial}{\partial r} \left( r^2 \frac{\partial \psi}{\partial r} \right) \Longrightarrow \frac{1}{r} \frac{\partial^2}{\partial r^2} (r \psi).
		\label{eq:lap_deriv}
	\end{align}
	% -----------
By multiplying \eq{eq:lap_deriv} with $r$ one can finally obtain a result that is very similar to that in \eq{eq:wave_equation}:
	% -----------
	\begin{align}
		\boxed{\frac{\partial^2}{\partial r^2} (r \psi) = \frac{1}{v^2} \frac{\partial^2}{\partial t^2} (r \psi)}
		\label{eq:lap_deriv2}
	\end{align}
	% -----------
In other words: we have derived an equation that has the same form as the \emph{1D wave equation}, except that the ``wave function'' is now denoted by $r \psi$. The solution to that is straight-forward and one obtains:
	% -----------
	\begin{align*}
		r \psi (r,t) &= f (r - v t)\\
		\Longrightarrow \psi (r,t) &= \frac{f (r - v t)}{r}
	\end{align*}
	% -----------
In analogy to \eq{eq:3d_general_solu} the general solution is given by:
	% -----------
	\begin{align}
		\psi (r,t) = c_1 \frac{f ( r - v t)}{r} + c_2 \frac{g ( r + v t)}{r}
	\end{align}
	% -----------
Finally, a special case, namely that of a \emph{harmonic spherical wave}, is notable and both of the following two notations are valid:
	% -----------
	\begin{align}
		\boxed{\psi (r,t) = \frac{A}{r} \cos [k (r \mp v t)]} && \Longleftrightarrow && \boxed{\psi (r,t) = \frac{A}{r} e^{i k (r \mp v t)}}
	\end{align}
	% -----------
The equations of the electromagnetic interaction in free space are analogous to \eq{eq:wave_equation} and \eq{eq:lap_deriv2}:
	% -----------
	\begin{align}
        \nabla^2 \vec{B} = \frac{1}{c^2} \frac{\partial^2
            \vec{B}}{\partial t^2} && \text{and} && \nabla^2 \vec{E} =
            \frac{1}{c^2} \frac{\partial^2 \vec{E}}{\partial t^2}
            \label{eq:wave_equation_maxwell}
	\end{align}
	% -----------
To derive \eq{eq:wave_equation_maxwell}, we start by recalling Maxwell's equations in the vacuum:
	% -----------
	\begin{align}
            \nabla \cdot \vec{E} = 0
            \label{eq:maxwell_equation_1}
	\end{align}
	% -----------
	\begin{align}
            \nabla \cdot \vec{B} = 0
            \label{eq:maxwell_equation_2}
	\end{align}
	% -----------
	\begin{align}
            \nabla \times \vec{E} = -\frac{\partial \vec{B}}{\partial t}
            \label{eq:maxwell_equation_3}
	\end{align}
	% -----------
	\begin{align}
            \nabla \times \vec{B} = \mu_{0}\epsilon_{0}\frac{\partial \vec{E}}{\partial t}
            \label{eq:maxwell_equation_4}
	\end{align}
	% -----------
where $\vec{E}$ and $\vec{B}$ are, respectively, the electric and the magnetic field;
$\epsilon_{0}$ and $\mu_{0}$ are, respectively, the electric permittivity
and the magnetic permeability of free space and it holds that $\epsilon_{0}\mu_{0} = c^{-2}$;
$(\nabla \cdot)$ is the divergence operator and $\nabla \cdot \vec{A} = \frac{\partial A}{\partial x} +
\frac{\partial A}{\partial y} + \frac{\partial A}{\partial z}$ (for any vector $\vec{A}$) is a scalar; $(\nabla \times)$
is the curl operator. The starting point is \eq{eq:maxwell_equation_3}, where the curl operator is applied to both sides of the equation:
	\begin{align}
	    \nabla \times \left( \nabla \times \vec{E} \right) = \nabla \times \left( -\frac{\partial \vec{B}}{\partial t} \right) 
            \label{eq:wave_equation_derivation_1}
	\end{align}
	% -----------
The left hand side of \eq{eq:wave_equation_derivation_1} is transformed by considering a property of the curl operator
and \eq{eq:maxwell_equation_1}:
	\begin{align}
	    \nabla \times \left( \nabla \times \vec{E} \right) = \nabla(\nabla \cdot \vec{E}) - \nabla^{2}\vec{E} =
	    - \nabla^{2}\vec{E}
            \label{eq:wave_equation_derivation_2}
	\end{align}
	% -----------
whereas, for the right hand side of \eq{eq:wave_equation_derivation_1}, the $\nabla$ and the $\partial/\partial t$ are exchanged
(since they operate on different variables) and \eq{eq:maxwell_equation_4} is used:
	\begin{align}
	    \nabla \times \left( -\frac{\partial \vec{B}}{\partial t} \right) = 
	    -\frac{\partial}{\partial t} \left( \nabla \times \vec{B} \right) = 
	    - \frac{\partial}{\partial t} \left( \mu_{0}\epsilon_{0}\frac{\partial \vec{E}}{\partial t} \right)  
            \label{eq:wave_equation_derivation_3}
	\end{align}
	% -----------
Substituting \eq{eq:wave_equation_derivation_2} and \eq{eq:wave_equation_derivation_3} to the left and right hand side of
\eq{eq:wave_equation_derivation_1}, the equation of the electric field for the free space propagation is derived:
	\begin{align}
	    -\nabla^2 \vec{E} =
	    \nabla \times \left( \nabla \times \vec{E} \right) =
	    \nabla \times \left( -\frac{\partial \vec{B}}{\partial t} \right) =
	    - \mu_{0}\epsilon_{0} \frac{\partial^{2} \vec{E}}{\partial t^{2}} =
	    - \frac{1}{c^{2}} \frac{\partial^{2} \vec{E}}{\partial t^{2}}
            \label{eq:wave_equation_derivation_4}
	\end{align}
	% -----------
In an analogous way, the equation of the magnetic field for the free space propagation can be obtained.
\newline
These equations have a simpler form after taking the Fourier transform with
respect to time, since the derivative $\partial/\partial t$ becomes a multiplication
by $i\omega$ (see Appendix~\ref{cha:fourier.transform}, derivative property of the Fourier transform).
The Fourier transform operator is applied to both side of \eq{eq:wave_equation_maxwell} for the electric field
(the procedure is, of course, identical for the magnetic field):
\begin{align}
  \Four\left(\nabla^2 \vec{E}\right) &= \Four\left(\frac{1}{c^{2}} \frac{\partial^{2} \vec{E}}{\partial t^{2}} \right) \\
                                     &= \frac{1}{c^{2}}\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty} \de t
                                        \: e^{-i \omega t} \left(\frac{\partial^{2}\vec{E}}{\partial t^{2}}\right) \\
         \text{integrating by parts} &= - \frac{1}{c^{2}}\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty} \de t \: 
                                        (-i\omega) e^{-i \omega t} \left(\frac{\partial\vec{E}}{\partial t}\right)\\
         \text{integrating by parts} &= \frac{1}{c^{2}}\frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty} \de t \: 
                                        (-i\omega)(-i\omega) e^{-i \omega t} \vec{E}\\  
                                     &= \frac{1}{\sqrt{2\pi}}\int_{-\infty}^{+\infty} \de t \: 
                                        e^{-i \omega t} \left( -\frac{\omega^{2}}{c^{2}} \vec{E} \right) = 
                                        \Four\left(-\frac{\omega^{2}}{c^{2}} \vec{E}\right)
\end{align}
The Fourier operator is, then, removed from both sides obtaining:
\begin{align}
  \nabla^2 \vec{E} = -\frac{\omega^{2}}{c^{2}} \vec{E}
\end{align}
Therefore, in the frequency domain, we have $\partial^2 / \partial t^2
\rightarrow \omega^2$ and we can write the \emph{Helmholtz equation}, which
is of particular importance for wave optics (see
section~\ref{sec:paraxial_geometry}):
	% -----------
	\begin{align}
        \boxed{\Big(\nabla^2 + \frac{\omega^2}{c^2}\Big) E_i = 0.}
	\end{align}
	% -----------
