The fabrication of conical surfaces is very difficult, while it is possible
to polish spherical surfaces very well. For investigating whether ``good
imaging'' is possible with spherical surfaces, we will work out the
\emph{lensmaker's equation} (see \fig{fig:spherical_surface}) and apply \emph{Fermat's principle}, which states that the derivative of the optical path length (OPL) with respect to the position variable must be zero (\see{cha:fermat_principle}).

%==================================================
\begin{figure}[ht]
	\centering
	\includegraphics[width=8cm]{graphics/spherical_surface.jpg}
	\caption{Refraction at a spherical interface. Conjugate foci}
	\label{fig:spherical_surface}
\end{figure}
%==================================================	

We begin by writing
	% -----------
	\begin{align*}
		\text{OPL} = n_1 l_0 + n_2 l_\mathrm{i}
	\end{align*}
	% -----------
and by using trigonometry we can write:
	% -----------
	\begin{align*}
		\left.
		\begin{array}{rr}
			l_0 = &\left[ R^2 + (s_0 + R)^2 - 2 R (s_0 + R) \cos \varphi \right]^{\frac{1}{2}} \\
			l_\mathrm{i} = & \left[ R^2 + (s_\mathrm{i} - R)^2 + 2 R (s_\mathrm{i} - R) \cos \varphi \right]^{\frac{1}{2}}
		\end{array}
		\right\rbrace \text{with $\varphi$ being the position variable}
	\end{align*}
	% -----------
By demanding $\frac{\mathrm{d} OPL}{\mathrm{d} \varphi} \stackrel{!}{=} 0$ one can show that
	% -----------
	\begin{align*}
		\frac{n_1 R (s_0 + R) \sin \varphi}{2 l_0} - \frac{n_2 R (s_\mathrm{i} - R) \sin \varphi}{2 l_\mathrm{i}} = 0
	\end{align*}
	% -----------
and therefore
	% -----------
	\begin{align*}
		\frac{n_1}{l_0} + \frac{n_2}{l_\mathrm{i}} = \frac{1}{R} \left( \frac{n_2 s_\mathrm{i}}{l_\mathrm{i}} - \frac{n_1 s_0}{l_0} \right).
	\end{align*}
	% -----------
By making use of the \emph{paraxial approximation}, \ie $\sin \varphi \approx \varphi$ and $\cos \varphi \approx 1$ we obtain:
	% -----------
	\begin{align*}
		f_0 \approx s_0 \qquad \text{and} \qquad f_i \approx s_i,
	\end{align*}
	% -----------
which finally gives:
	% -----------
	\begin{align}
		\boxed{\frac{n_1}{s_0} + \frac{n_2}{s_\mathrm{i}} = \frac{n_2 - n_\mathrm{i}}{R}}
	\end{align}
	% -----------
We finally get:
	\begin{align*}
		s_\mathrm{i} &= \infty \Longrightarrow s_0 := f_0 = \frac{n_1}{n_2 - n_1} R \qquad \text{object focal length} \\
		s_0 &= \infty \Longrightarrow s_\mathrm{i} := f_\mathrm{i} = \frac{n_2}{n_2 - n_1} R \qquad \text{image focal length} \\
	\end{align*}
	% -----------

%==================================================
\begin{figure}[ht]
	\centering
	\includegraphics[width=8cm]{graphics/spherical_lens.jpg}
	\caption{A spherical lens. (a) Rays in a vertical plane passing through
        a lens. Conjugate foci. (b) Refraction at the interfaces. The radius drawn from $C_1$ is normal to the first surface, and as the ray enters the lens it bends down \emph{toward} that normal. The radius from $C_2$ is normal to the second surface; and as the ray emerges, since $n_\mathrm{l} > n_\mathrm{a}$, the ray bends down \emph{away} from that normal. (c) The geometry}
	\label{fig:spherical_lens}
\end{figure}
%==================================================

If one wants to image an object in one medium onto another medium, the lens should have a second surface (see \fig{fig:spherical_lens}). In such a case the equation becomes:
	\begin{align*}
		\frac{n_\mathrm{m}}{s_{01}} + \frac{n_\mathrm{m}}{s_{\mathrm{i} 2}} = (n_\mathrm{l} - n_\mathrm{m}) \cdot \left( \frac{1}{R_1} - \frac{1}{R_2} \right) + \frac{n_\mathrm{l} d}{(s_{i 1} - d) s_{i 1}},
	\end{align*}
	% -----------
where $n_\mathrm{m}$ is the index of the surrounding medium, $n_\mathrm{l}$ the index of the lens and $R_1$, $R_2$ the radii of the two lens surfaces. By taking the limit $d \rightarrow 0$ and $n_\mathrm{m} \approx 1$ we arrive at the so-called \emph{lensmaker's equation}:
	% -----------
	\begin{align}
		\boxed{\frac{1}{s_0} + \frac{1}{s_\mathrm{i}} = (n_\mathrm{l} -1) \cdot \left( \frac{1}{R_1} - \frac{1}{R_2} \right)}
	\end{align}
	% -----------
Another form is valid for $s_0 \rightarrow \infty$ and $s_\mathrm{i} \rightarrow \infty$:
	% -----------
	\begin{align}
		\boxed{\frac{1}{s_0} + \frac{1}{s_\mathrm{i}} = \frac{1}{f}}
	\end{align}
	% -----------
For magnifications and sign conventions, see \fig{fig:slide17}.

%==================================================
\begin{figure}[ht]
	\centering
	\includegraphics[width=\linewidth]{graphics/slide17v2}
	\caption{Magnification and signs conventions}
	\label{fig:slide17}
\end{figure}
%==================================================	

\clearpage
