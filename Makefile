LL=latexmk -pdf
CLEAN=latexmk -C

.PHONY: all publish

all: master.pdf master_ex.pdf master_ex_sol.pdf

master.pdf: master.tex 01_header.tex 02_title.tex 03_title_inside.tex macros.sty revision.tex $(wildcard chapters/*.tex graphics/*)
	${LL} -pdf $<

master_ex.pdf: master_ex.tex 01_header_ex.tex macros.sty $(wildcard chapters_ex/*.tex graphics_ex/*)
	${LL} -pdf $<

master_ex_sol.pdf: master_ex_sol.tex 01_header_ex.tex macros.sty $(wildcard chapters_ex_solu/*.tex)
	${LL} -pdf $<

.git/hooks/post-commit: post-commit
	ln -s ../../post-commit .git/hooks/post-commit

clean:
	$(CLEAN)
	rm -f revision.tex

revision.tex: post-commit
	./post-commit
